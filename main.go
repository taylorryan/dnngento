package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/taylorryan/dnngento/download"
	"gitlab.com/taylorryan/dnngento/magento"
	"gitlab.com/taylorryan/dnngento/parsing"
)

func main() {

	f, err := os.OpenFile("dnngento.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()

	log.SetOutput(f)

	ordersFlag := flag.Bool("orders", false, "run import processing")
	usersFlag := flag.Bool("users", false, "run users import processing")
	apiFlag := flag.Bool("api", false, "run api processing")
	runFlag := flag.Bool("run", false, "run processing")
	docFlag := flag.Bool("doc", false, "run document parsing for instruction pdfs")
	flag.Parse()
	log.Println("Start")

	if *ordersFlag {
		importOrders()
	} else if *usersFlag {
		importUsers()
	} else if *apiFlag {
		magento.ApiPostUsers()
	} else if *runFlag {
		runMigration()
	} else if *docFlag {
		parsePdfs()
	}

	log.Println("Done")
}

func importFiles() {
	orderLines := parsing.ParseOrder("orders.csv")
	orderDetailLines := parsing.ParseOrderDetails("order_details.csv")
	// userLines := parsing.ParseUsers("users.csv")
	// userAddressLines := parsing.ParseUserAddresses("address.csv")

	orders := []*magento.Order{}

	for _, line := range orderLines {
		o := magento.CreateOrder(line)
		orders = append(orders, o)
	}

	log.Println("# of orders: ", len(orders))

	lineitems := []*magento.LineItem{}

	for _, line := range orderDetailLines {
		li := magento.CreateLineItem(line)
		lineitems = append(lineitems, li)
	}

	log.Println("# of line items: ", len(lineitems))

	for _, lineitem := range lineitems {
		needle := lineitem.OrderID
		idx := find(orders, needle)
		if idx < len(orders) && orders[idx].DnnID == needle {
			orders[idx].LineItems = append(orders[idx].LineItems, lineitem)
		}

	}

	magento.WriteMagentoImportFile(orders)

	// users := []*magento.User{}
	// for _, line := range userLines {
	// 	user := magento.CreateUser(line)
	// 	users = append(users, user)
	// }

	// log.Println("# of users: ", len(users))
	// log.Println("# of addresses: ", len(userAddressLines))

	// magento.AddAddressesToUser(userAddressLines, users)

	// magento.WriteMagentoUserImportFile(users)
}

func importUsers() []*magento.User {
	userLines := parsing.ParseUsers("users.csv")
	userAddressLines := parsing.ParseUserAddresses("address.csv")

	users := []*magento.User{}
	for _, line := range userLines {
		user := magento.CreateUser(line)
		users = append(users, user)
	}

	log.Println("# of users: ", len(users))
	log.Println("# of addresses: ", len(userAddressLines))

	magento.AddAddressesToUser(userAddressLines, users)

	magento.WriteMagentoUserImportFile(users)

	return users
}

func importOrders() {
	users := magento.ApiGetUsers()

	orders := parseOrders()

	for _, order := range orders {
		// Map user to order
		orderEmail := order.Email
		user := users[orderEmail]
		if user != nil {
			i, _ := strconv.Atoi(user["entity_id"].(string))

			order.CustomerID = i
		}
	}

	magento.WriteMagentoImportFile(orders)
}

func find(a []*magento.Order, x string) int {
	for i, n := range a {
		if x == n.DnnID {
			return i
		}
	}
	return -1
}

func runMigration() {
	users := importUsers()

	// Create users in magento
	magento.XmlCreateUsers(users)

	log.Println("Processed all users: ", len(users))

	// Parse Orders
	orders := parseOrders()

	// Create map of users (key is dnn id)
	userMap := make(map[string]int)
	for _, user := range users {
		userMap[user.DnnID] = user.ID
	}

	// Link order to new Magento User id
	for _, order := range orders {
		// Map user to order
		newID := userMap[order.DnnCustomerID]
		order.CustomerID = newID
	}

	magento.WriteMagentoImportFile(orders)
}

func parseOrders() []*magento.Order {
	orderLines := parsing.ParseOrder("orders.csv")
	orderDetailLines := parsing.ParseOrderDetails("order_details.csv")

	orders := []*magento.Order{}

	for _, line := range orderLines {

		if strings.Contains(line.SalesOrderNumber, "CART") {
			// Order is actually a cart, ignore
			continue
		}

		o := magento.CreateOrder(line)
		orders = append(orders, o)
	}

	log.Println("# of orders: ", len(orders))

	lineitems := []*magento.LineItem{}

	for _, line := range orderDetailLines {
		li := magento.CreateLineItem(line)
		lineitems = append(lineitems, li)
	}

	log.Println("# of line items: ", len(lineitems))

	for _, lineitem := range lineitems {
		needle := lineitem.OrderID
		idx := find(orders, needle)
		if idx == -1 {
			continue
		}
		if idx < len(orders) && orders[idx].DnnID == needle {
			orders[idx].LineItems = append(orders[idx].LineItems, lineitem)
		}

	}

	return orders
}

func parsePdfs() {
	re := regexp.MustCompile(`\/[\w\s/.-]*\.pdf`)

	dat, err := ioutil.ReadFile("pdf.txt")
	if err != nil {
		panic(err)
	}

	matches := re.FindAll(dat, -1)

	for _, line := range matches {
		url := fmt.Sprintf("https://astrotool.com%v", string(line))
		splitUrl := strings.Split(url, "/")
		filepath := fmt.Sprintf("pdfs/%v", splitUrl[len(splitUrl)-1])

		if err := download.DownloadFile(filepath, url); err != nil {
			fmt.Println(err)
		}
	}

}
