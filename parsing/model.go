package parsing

import (
	"strings"
	"time"
)

// DateTime struct - Allow for better parsing of CSV
type DateTime struct {
	time.Time
}

// MarshalCSV - Convert the internal date as CSV string
func (date *DateTime) MarshalCSV() (string, error) {
	return date.Time.Format("1/2/2006 3:04:05 PM"), nil
}

// UnmarshalCSV - Convert the CSV string as internal date
func (date *DateTime) UnmarshalCSV(csv string) (err error) {
	s := strings.TrimSpace(csv)
	if len(s) > 0 {
		date.Time, err = time.Parse("1/2/2006 3:04:05 PM", csv)
	}
	return err
}

// DnnOrderLine - Order Line from DNN
type DnnOrderLine struct {
	SalesOrderID                 string
	SalesOrderGUID               string
	UserID                       string
	CreateDate                   *DateTime
	BillingFirstName             string
	BillingLastName              string
	BillingCompany               string
	BillingStreet                string
	BillingCity                  string
	BillingSubdivisionCode       string
	BillingPostalCode            string
	BillingCountryCode           string
	BillingPhone                 string
	BillingEmail                 string
	ShippingFirstName            string
	ShippingLastName             string
	ShippingCompany              string
	ShippingStreet               string
	ShippingCity                 string
	ShippingSubdivisionCode      string
	ShippingPostalCode           string
	ShippingCountryCode          string
	ShippingPhone                string
	ShippingEmail                string
	PortalID                     string
	ShippingAmount               float64
	SubTotalAmount               float64
	TotalAmount                  float64
	AdminNotes                   string
	UpdateDate                   *DateTime
	UserHostAddress              string
	Status                       string
	HandlingAmount               float64
	OrderDate                    *DateTime
	TaxAmount1                   float64
	TaxAmount2                   float64
	TaxAmount3                   float64
	TaxAmount4                   float64
	TaxAmount5                   float64
	ShippingDiscountAmount       float64
	HandlingDiscountAmount       float64
	TaxDiscountAmount            float64
	CouponCodes                  string
	ShippingStatus               string
	DynamicFormResult            string
	SalesPaymentStatus           string
	HandlingMethodID             string
	ShippingMethodID             string
	CultureCode                  string
	ExchangeRate                 string
	PreferredUserPaymentID       string
	PurchaseOrderNumber          string
	Origin                       string
	ShippingTaxAmount1           float64
	ShippingTaxAmount2           float64
	ShippingTaxAmount3           float64
	ShippingTaxAmount4           float64
	ShippingTaxAmount5           float64
	HandlingTaxAmount1           float64
	HandlingTaxAmount2           float64
	HandlingTaxAmount3           float64
	HandlingTaxAmount4           float64
	HandlingTaxAmount5           float64
	ShippingTrackingCode         string
	CustomerNotes                string
	ShippingUniversalServiceName string
	BusinessTaxNumber            string
	WishListID                   string
	PackingMethodID              string
	CartAbandonNotified          string
	OrderLocked                  string
	RewardsPointsRewarded        string
	RewardsPointsQualified       string
	ShippedDate                  *DateTime
	AffiliateID                  string
	FraudScore                   string
	FraudRiskGateway             string
	CurrencyCultureCode          string
	SalesOrderNumber             string
	SellerID                     string
	WarehouseID                  string
	ParentSalesOrderID           string
	ShippingPackages             string
}

// DnnOrderDetailsLine - Order Details Line from DNN
type DnnOrderDetailsLine struct {
	SalesOrderDetailID       string
	SalesOrderID             string
	ProductVariantID         string
	Quantity                 int
	Price                    float64
	CreateDate               *DateTime
	UpdateDate               *DateTime
	ProductName              string
	ProductVariantName       string
	SKU                      string
	ProductCost              float64
	RecurringInterval        string
	RecurringIntervalType    string
	Weight                   float64
	Depth                    float64
	Width                    float64
	Height                   float64
	RequireShipping          string
	ProductVariantExtension  string
	DiscountAmount           float64
	DynamicFormResult        string
	TaxAmount1               float64
	TaxAmount2               float64
	TaxAmount3               float64
	TaxAmount4               float64
	TaxAmount5               float64
	PackageType              string
	BasePrice                float64
	RequireHandling          string
	ShippingPrice            float64
	HandlingPrice            float64
	RecurringSalesOrderID    string
	ShippingStatus           string
	ParentSalesOrderDetailID string
	ProductPartID            string
	Status                   string
	AdminNotes               string
	BookingStartDate         *DateTime
	BookingStopDate          *DateTime
}

// MagentoImportLine - Order Line to import into magento
type MagentoImportLine struct {
	OrderID                    string    `csv:"client_id"`
	Email                      string    `csv:"email"`
	Firstname                  string    `csv:"firstname"`
	Lastname                   string    `csv:"lastname"`
	Prefix                     string    `csv:"prefix"`
	Middlename                 string    `csv:"middlename"`
	Suffix                     string    `csv:"suffix"`
	Taxvat                     string    `csv:"taxvat"`
	CreatedAt                  *DateTime `csv:"created_at"`
	UpdatedAt                  *DateTime `csv:"updated_at"`
	InvoiceCreatedAt           *DateTime `csv:"invoice_created_at"`
	ShipmentCreatedAt          *DateTime `csv:"shipment_created_at"`
	CreditmemoCreatedAt        *DateTime `csv:"creditmemo_created_at"`
	TaxAmount                  float64   `csv:"tax_amount"`
	BaseTaxAmount              float64   `csv:"base_tax_amount"`
	DiscountAmount             float64   `csv:"discount_amount"`
	BaseDiscountAmount         float64   `csv:"base_discount_amount"`
	ShippingTaxAmount          float64   `csv:"shipping_tax_amount"`
	BaseShippingTaxAmount      float64   `csv:"base_shipping_tax_amount"`
	BaseToGlobalRate           float64   `csv:"base_to_global_rate"`
	BaseToOrderRate            float64   `csv:"base_to_order_rate"`
	StoreToBaseRate            float64   `csv:"store_to_base_rate"`
	StoreToOrderRate           float64   `csv:"store_to_order_rate"`
	SubtotalInclTax            float64   `csv:"subtotal_incl_tax"`
	BaseSubtotalInclTax        float64   `csv:"base_subtotal_incl_tax"`
	CouponCode                 string    `csv:"coupon_code"`
	ShippingInclTax            float64   `csv:"shipping_incl_tax"`
	BaseShippingInclTax        float64   `csv:"base_shipping_incl_tax"`
	ShippingMethod             string    `csv:"shipping_method"`
	ShippingAmount             float64   `csv:"shipping_amount"`
	Subtotal                   float64   `csv:"subtotal"`
	BaseSubtotal               float64   `csv:"base_subtotal"`
	GrandTotal                 float64   `csv:"grand_total"`
	BaseGrandTotal             float64   `csv:"base_grand_total"`
	BaseShippingAmount         float64   `csv:"base_shipping_amount"`
	AdjustmentPositive         float64   `csv:"adjustment_positive"`
	AdjustmentNegative         float64   `csv:"adjustment_negative"`
	RefundedShippingAmount     float64   `csv:"refunded_shipping_amount"`
	BaseRefundedShippingAmount float64   `csv:"base_refunded_shipping_amount"`
	RefundedSubtotal           float64   `csv:"refunded_subtotal"`
	BaseRefundedSubtotal       float64   `csv:"base_refunded_subtotal"`
	RefundedTaxAmount          float64   `csv:"refunded_tax_amount"`
	BaseRefundedTaxAmount      float64   `csv:"base_refunded_tax_amount"`
	RefundedDiscountAmount     float64   `csv:"refunded_discount_amount"`
	BaseRefundedDiscountAmount float64   `csv:"base_refunded_discount_amount"`
	StoreID                    string    `csv:"store_id"`
	OrderStatus                string    `csv:"order_status"`
	OrderState                 string    `csv:"order_state"`
	HoldBeforeState            string    `csv:"hold_before_state"`
	HoldBeforeStatus           string    `csv:"hold_before_status"`
	StoreCurrencyCode          string    `csv:"store_currency_code"`
	BaseCurrencyCode           string    `csv:"base_currency_code"`
	OrderCurrencyCode          string    `csv:"order_currency_code"`
	TotalPaid                  float64   `csv:"total_paid"`
	BaseTotalPaid              float64   `csv:"base_total_paid"`
	IsVirtual                  int       `csv:"is_virtual"`
	TotalQtyOrdered            int       `csv:"total_qty_ordered"`
	RemoteIP                   string    `csv:"remote_ip"`
	TotalRefunded              float64   `csv:"total_refunded"`
	BaseTotalRefunded          float64   `csv:"base_total_refunded"`
	TotalCanceled              float64   `csv:"total_canceled"`
	TotalInvoiced              float64   `csv:"total_invoiced"`
	BillingCustomerID          string    `csv:"customer_id"`
	BillingPrefix              string    `csv:"billing_prefix"`
	BillingFirstname           string    `csv:"billing_firstname"`
	BillingMiddlename          string    `csv:"billing_middlename"`
	BillingLastname            string    `csv:"billing_lastname"`
	BillingSuffix              string    `csv:"billing_suffix"`
	BillingStreetFull          string    `csv:"billing_street_full"`
	BillingCity                string    `csv:"billing_city"`
	BillingRegion              string    `csv:"billing_region"`
	BillingCountry             string    `csv:"billing_country"`
	BillingPostcode            string    `csv:"billing_postcode"`
	BillingTelephone           string    `csv:"billing_telephone"`
	BillingCompany             string    `csv:"billing_company"`
	BillingFax                 string    `csv:"billing_fax"`
	ShippingCustomerID         string    `csv:"customer_id"`
	ShippingPrefix             string    `csv:"shipping_prefix"`
	ShippingFirstname          string    `csv:"shipping_firstname"`
	ShippingMiddlename         string    `csv:"shipping_middlename"`
	ShippingLastname           string    `csv:"shipping_lastname"`
	ShippingSuffix             string    `csv:"shipping_suffix"`
	ShippingStreetFull         string    `csv:"shipping_street_full"`
	ShippingCity               string    `csv:"shipping_city"`
	ShippingRegion             string    `csv:"shipping_region"`
	ShippingCountry            string    `csv:"shipping_country"`
	ShippingPostcode           string    `csv:"shipping_postcode"`
	ShippingTelephone          string    `csv:"shipping_telephone"`
	ShippingCompany            string    `csv:"shipping_company"`
	ShippingFax                string    `csv:"shipping_fax"`
	PaymentMethod              string    `csv:"payment_method"`
	ProductSku                 string    `csv:"product_sku"`
	ProductName                string    `csv:"product_name"`
	QtyOrdered                 int       `csv:"qty_ordered"`
	QtyInvoiced                int       `csv:"qty_invoiced"`
	QtyShipped                 int       `csv:"qty_shipped"`
	QtyRefunded                int       `csv:"qty_refunded"`
	QtyCanceled                int       `csv:"qty_canceled"`
	ProductType                string    `csv:"product_type"`
	OriginalPrice              float64   `csv:"original_price"`
	BaseOriginalPrice          float64   `csv:"base_original_price"`
	RowTotal                   float64   `csv:"row_total"`
	BaseRowTotal               float64   `csv:"base_row_total"`
	RowWeight                  float64   `csv:"row_weight"`
	PriceInclTax               float64   `csv:"price_incl_tax"`
	BasePriceInclTax           float64   `csv:"base_price_incl_tax"`
	ProductTaxAmount           float64   `csv:"product_tax_amount"`
	ProductBaseTaxAmount       float64   `csv:"product_base_tax_amount"`
	ProductTaxPercent          float64   `csv:"product_tax_percent"`
	ProductDiscount            float64   `csv:"product_discount"`
	ProductBaseDiscount        float64   `csv:"product_base_discount"`
	ProductDiscountPercent     float64   `csv:"product_discount_percent"`
	IsChild                    string    `csv:"is_child"`
	ProductOption              string    `csv:"product_option"`
}

// DnnUserLine - User Line from DNN
type DnnUserLine struct {
	UserID                  string
	Username                string
	FirstName               string
	LastName                string
	IsSuperUser             string
	AffiliateID             string `csv:"AffiliateId"`
	Email                   string
	DisplayName             string
	UpdatePassword          string
	LastIPAddress           string
	IsDeleted               string
	CreatedByUserID         string
	CreatedOnDate           DateTime
	LastModifiedByUserID    string
	LastModifiedOnDate      DateTime
	PasswordResetToken      string
	PasswordResetExpiration string
	LowerEmail              string
}

// DnnUserLine - User Address Line from DNN
type DnnUserAddressLine struct {
	UserAddressID   string
	UserID          string
	PortalID        string
	PrimaryShipping string
	PrimaryBilling  string
	FirstName       string
	LastName        string
	Company         string
	Street          string
	City            string
	SubdivisionCode string
	PostalCode      string
	CountryCode     string
	Phone           string
	Email           string
	CreateDate      string
	UpdateDate      string
}

// MagentoUserLine - User Line to import into magento. Uses Profile Import
type MagentoUserLine struct {
	Website                string `csv:"website"`
	Email                  string `csv:"email"`
	GroupID                string `csv:"group_id"`
	DisableAutoGroupChange string `csv:"disable_auto_group_change"`
	Firstname              string `csv:"firstname"`
	Lastname               string `csv:"lastname"`
	PasswordHash           string `csv:"password_hash"`
	Telephone              string `csv:"telephone"`
	URL                    string `csv:"url"`
	Newsletter             string `csv:"newsletter"`
	Fax                    string `csv:"fax"`
	BillingPrefix          string `csv:"billing_prefix"`
	BillingFirstname       string `csv:"billing_firstname"`
	BillingMiddlename      string `csv:"billing_middlename"`
	BillingLastname        string `csv:"billing_lastname"`
	BillingSuffix          string `csv:"billing_suffix"`
	BillingStreetFull      string `csv:"billing_street_full"`
	BillingStreet1         string `csv:"billing_street1"`
	BillingStreet2         string `csv:"billing_street2"`
	BillingStreet3         string `csv:"billing_street3"`
	BillingStreet4         string `csv:"billing_street4"`
	BillingStreet5         string `csv:"billing_street5"`
	BillingStreet6         string `csv:"billing_street6"`
	BillingStreet7         string `csv:"billing_street7"`
	BillingStreet8         string `csv:"billing_street8"`
	BillingCity            string `csv:"billing_city"`
	BillingRegion          string `csv:"billing_region"`
	BillingCountry         string `csv:"billing_country"`
	BillingPostcode        string `csv:"billing_postcode"`
	BillingTelephone       string `csv:"billing_telephone"`
	BillingCompany         string `csv:"billing_company"`
	BillingFax             string `csv:"billing_fax"`
	ShippingPrefix         string `csv:"shipping_prefix"`
	ShippingFirstname      string `csv:"shipping_firstname"`
	ShippingMiddlename     string `csv:"shipping_middlename"`
	ShippingLastname       string `csv:"shipping_lastname"`
	ShippingSuffix         string `csv:"shipping_suffix"`
	ShippingStreetFull     string `csv:"shipping_street_full"`
	ShippingStreet1        string `csv:"shipping_street1"`
	ShippingStreet2        string `csv:"shipping_street2"`
	ShippingStreet3        string `csv:"shipping_street3"`
	ShippingStreet4        string `csv:"shipping_street4"`
	ShippingStreet5        string `csv:"shipping_street5"`
	ShippingStreet6        string `csv:"shipping_street6"`
	ShippingStreet7        string `csv:"shipping_street7"`
	ShippingStreet8        string `csv:"shipping_street8"`
	ShippingCity           string `csv:"shipping_city"`
	ShippingRegion         string `csv:"shipping_region"`
	ShippingCountry        string `csv:"shipping_country"`
	ShippingPostcode       string `csv:"shipping_postcode"`
	ShippingTelephone      string `csv:"shipping_telephone"`
	ShippingCompany        string `csv:"shipping_company"`
	ShippingFax            string `csv:"shipping_fax"`
	CreatedIn              string `csv:"created_in"`
	IsSubscribed           string `csv:"is_subscribed"`
	Group                  string `csv:"group"`
	Confirmation           string `csv:"confirmation"`
	RpToken                string `csv:"rp_token"`
	RpTokenCreatedAt       string `csv:"rp_token_created_at"`
	Middlename             string `csv:"middlename"`
}

// MagentoUserLine - User Line to import into magento v2. Uses Import
type MagentoUserImportLine struct {
	Email                  string `csv:"email"`
	Website                string `csv:"_website"`
	Store                  string `csv:"_store"`
	Confirmation           string `csv:"confirmation"`
	CreatedAt              string `csv:"created_at"`
	CreatedIn              string `csv:"created_in"`
	DisableAutoGroupChange string `csv:"disable_auto_group_change"`
	Dob                    string `csv:"dob"`
	Fax                    string `csv:"fax"`
	Firstname              string `csv:"firstname"`
	Gender                 string `csv:"gender"`
	GroupID                int    `csv:"group_id"`
	Lastname               string `csv:"lastname"`
	Middlename             string `csv:"middlename"`
	Newsletter             string `csv:"newsletter"`
	passwordHash           string `csv:"password_hash"`
	Prefix                 string `csv:"prefix"`
	rpToken                string `csv:"rp_token"`
	rpTokenCreatedAt       string `csv:"rp_token_created_at"`
	StoreID                int    `csv:"store_id"`
	Suffix                 string `csv:"suffix"`
	Taxvat                 string `csv:"taxvat"`
	Telephone              string `csv:"telephone"`
	URL                    string `csv:"url"`
	WebsiteID              int    `csv:"website_id"`
	password               string `csv:"password"`
	AddressCity            string `csv:"_address_city"`
	AddressCompany         string `csv:"_address_company"`
	AddressCountryID       string `csv:"_address_country_id"`
	AddressFax             string `csv:"_address_fax"`
	AddressFirstname       string `csv:"_address_firstname"`
	AddressLastname        string `csv:"_address_lastname"`
	AddressMiddlename      string `csv:"_address_middlename"`
	AddressPostcode        string `csv:"_address_postcode"`
	AddressPrefix          string `csv:"_address_prefix"`
	AddressRegion          string `csv:"_address_region"`
	AddressStreet          string `csv:"_address_street"`
	AddressSuffix          string `csv:"_address_suffix"`
	AddressTelephone       string `csv:"_address_telephone"`
	AddressVatID           string `csv:"_address_vat_id"`
	AddressDefaultBilling  string `csv:"_address_default_billing_"`
	AddressDefaultShipping string `csv:"_address_default_shipping_"`
}
