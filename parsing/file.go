package parsing

import (
	"os"

	"github.com/gocarina/gocsv"
)

// ParseOrder takes a file name and grabs the DnnOrderLines from it
func ParseOrder(filename string) []*DnnOrderLine {
	linesFile, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer linesFile.Close()

	lines := []*DnnOrderLine{}

	if err := gocsv.UnmarshalFile(linesFile, &lines); err != nil { // Load lines from file
		panic(err)
	}

	return lines
}

// ParseOrderDetails takes a file name and grabs the DnnOrderDetailsLines from it
func ParseOrderDetails(filename string) []*DnnOrderDetailsLine {
	linesFile, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer linesFile.Close()

	lines := []*DnnOrderDetailsLine{}

	if err := gocsv.UnmarshalFile(linesFile, &lines); err != nil { // Load lines from file
		panic(err)
	}

	return lines
}

// ParseUsers takes a file name and grabs the DnnUserLines from it
func ParseUsers(filename string) []*DnnUserLine {
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := []*DnnUserLine{}

	if err := gocsv.UnmarshalFile(file, &lines); err != nil { // Load lines from file
		panic(err)
	}

	return lines
}

// ParseUserAddresses takes a file name and grabs the DnnUserLines from it
func ParseUserAddresses(filename string) []*DnnUserAddressLine {
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := []*DnnUserAddressLine{}

	if err := gocsv.UnmarshalFile(file, &lines); err != nil { // Load lines from file
		panic(err)
	}

	return lines
}
