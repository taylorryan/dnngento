package magento

import (
	"gitlab.com/taylorryan/dnngento/parsing"
)

// Order struct
type Order struct {
	ID                string
	DnnID             string
	SalesOrderNumber  string
	Email             string
	Firstname         string
	Lastname          string
	CreatedAt         *parsing.DateTime
	UpdatedAt         *parsing.DateTime
	TaxAmount         float64
	DiscountAmount    float64
	ShippingTaxAmount float64
	BaseToGlobalRate  float64
	BaseToOrderRate   float64
	StoreToBaseRate   float64
	StoreToOrderRate  float64
	SubtotalInclTax   float64
	CouponCode        string
	ShippingInclTax   float64
	ShippingMethod    string
	ShippingAmount    float64
	Subtotal          float64
	GrandTotal        float64
	StoreID           int
	OrderStatus       string
	OrderState        string
	TotalQtyOrdered   int
	DnnCustomerID     string
	CustomerID        int
	BillingAddress    *Address
	ShippingAddress   *Address
	PaymentMethod     string

	LineItems []*LineItem
}

// Address struct
type Address struct {
	Prefix     string
	Firstname  string
	Middlename string
	Lastname   string
	Suffix     string
	Street     string
	City       string
	Region     string
	Country    string
	Postcode   string
	Telephone  string
	Company    string
	Fax        string
	Email      string
}

// LineItem struct
type LineItem struct {
	ID               string
	OrderID          string
	ProductSku       string
	ProductName      string
	QtyOrdered       int
	Price            float64
	RowTotal         float64
	RowWeight        float64
	PriceInclTax     float64
	ProductTaxAmount float64
	ProductOption    string
}

// User struct
type User struct {
	ID         int
	DnnID      string
	Firstname  string
	Middlename string
	Lastname   string
	Email      string
	Telephone  string
	Addresses  []*Address
}
