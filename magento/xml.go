package magento

import (
	"log"
	"strconv"

	xmlrpc "github.com/SHyx0rmZ/go-xmlrpc"
	"github.com/google/uuid"
)

func XmlCreateUsers(users []*User) {
	client := xmlrpc.NewClient("https://www.cms-tools.com/api/xmlrpc/")
	sessionID := xmlLogin(client).String()

	log.Println("Processing Users")
	count := 0
	for _, u := range users {
		id := xmlCreateUser(client, sessionID, *u)

		if id > 0 {
			u.ID = id

			// for i, address := range u.Addresses {
			// 	xmlCreateUserAddress(client, sessionID, u.ID, *address, (i == 0))
			// }
		}
		count++

		if count%100 == 0 {
			log.Println(count, "of", len(users))
		}
	}

	log.Println(count, "of", len(users))
}

func xmlFindUserByEmail(client xmlrpc.Client, sessionID string, email string) int {

	var filterArray []interface{}

	emailComparison := make(map[string]interface{})
	emailComparison["eq"] = email

	emailFilter := make(map[string]interface{})
	emailFilter["email"] = emailComparison

	filterArray = append(filterArray, emailFilter)

	users, err := client.Call("call", sessionID, "customer.list", filterArray)
	if err != nil {
		log.Println(err)
		return -1
	}

	if len(users.Values()) == 0 {
		return -1
	}

	foundUserProperties := users.Values()[0].Members()

	for _, s := range foundUserProperties {
		if s.Name() == "customer_id" {
			i, err := strconv.Atoi(s.Value().String())
			if err == nil {
				return i
			}
		}
	}
	return -1
}

func xmlCreateUser(client xmlrpc.Client, sessionID string, user User) int {
	xmlUser := make(map[string]interface{})
	xmlUser["email"] = user.Email
	xmlUser["firstname"] = user.Firstname
	xmlUser["lastname"] = user.Lastname
	xmlUser["password"] = uuid.New().String()
	xmlUser["website_id"] = 2
	xmlUser["store_id"] = 2
	xmlUser["group_id"] = 1

	existingUserID := xmlFindUserByEmail(client, sessionID, user.Email)

	if existingUserID >= 0 {
		// User exists, just return that ID
		return existingUserID
	}

	users := make([]map[string]interface{}, 1)
	users[0] = xmlUser

	customerID, err := client.Call("call", sessionID, "customer.create", users)
	if err != nil {
		log.Println(err)
		return -1
	}

	i, err := strconv.Atoi(customerID.String())
	if err != nil {
		log.Println(err)
		return -1
	}
	return i
}

func xmlLogin(client xmlrpc.Client) xmlrpc.Value {
	sessionID, err := client.Call("login", "taylor", "1234567890")
	if err != nil {
		log.Fatal(err)
	}

	return sessionID
}

func xmlCreateUserAddress(client xmlrpc.Client, sessionID string, userID int, address Address, isDefault bool) int {

	adder := xmlAddress(address)
	if isDefault {
		adder["is_default_billing"] = true
		adder["is_default_shipping"] = true
	}
	body := make([]interface{}, 2)
	body[0] = userID
	body[1] = adder

	id, err := client.Call("call", sessionID, "customer_address.create", body)
	if err != nil {
		log.Println(err)
	}
	return id.Int()
}

func xmlAddress(address Address) map[string]interface{} {
	xmlAddy := make(map[string]interface{})
	xmlAddy["city"] = address.City
	xmlAddy["company"] = address.Company
	xmlAddy["country_id"] = address.Country
	xmlAddy["fax"] = address.Fax
	xmlAddy["firstname"] = address.Firstname
	xmlAddy["lastname"] = address.Lastname
	xmlAddy["middlename"] = address.Middlename
	xmlAddy["postcode"] = address.Postcode
	xmlAddy["prefix"] = address.Prefix
	// xmlAddy["region_id"] = address.Region
	xmlAddy["region"] = address.Region
	streets := make([]string, 1)
	streets[0] = address.Street
	xmlAddy["street"] = streets
	xmlAddy["suffix"] = address.Suffix
	xmlAddy["telephone"] = address.Telephone

	addresses := make([]map[string]interface{}, 1)
	addresses[0] = xmlAddy

	return xmlAddy
}
