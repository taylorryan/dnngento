package magento

import (
	"fmt"
	"strings"

	"gitlab.com/taylorryan/dnngento/parsing"
)

// CreateOrder takes a csv line and converts it to a Magento Order
func CreateOrder(csvOrder *parsing.DnnOrderLine) *Order {

	order := &Order{}
	order.ID = fmt.Sprintf("DNN%v", csvOrder.SalesOrderID)
	order.DnnID = csvOrder.SalesOrderID
	order.SalesOrderNumber = csvOrder.SalesOrderNumber
	order.Email = csvOrder.BillingEmail
	order.Firstname = csvOrder.BillingFirstName
	order.Lastname = csvOrder.BillingLastName
	order.CreatedAt = csvOrder.CreateDate
	order.UpdatedAt = csvOrder.UpdateDate
	// order.TaxAmount
	// order.DiscountAmount
	// order.ShippingTaxAmount
	order.BaseToGlobalRate = 1.00
	order.BaseToOrderRate = 1.00
	order.StoreToBaseRate = 1.00
	order.StoreToOrderRate = 1.00
	order.SubtotalInclTax = csvOrder.SubTotalAmount
	order.CouponCode = csvOrder.CouponCodes
	order.ShippingInclTax = csvOrder.ShippingAmount
	order.ShippingMethod = csvOrder.ShippingUniversalServiceName
	order.ShippingAmount = csvOrder.ShippingAmount
	order.Subtotal = csvOrder.SubTotalAmount
	order.GrandTotal = csvOrder.TotalAmount
	// order.StoreID
	// order.OrderStatus
	// order.OrderState
	// order.TotalQtyOrdered
	order.DnnCustomerID = csvOrder.UserID
	// order.CustomerID = csvOrder.UserID
	// order.PaymentMethod = ??
	order.BillingAddress = getBillingAddress(csvOrder)
	order.ShippingAddress = getShippingAddress(csvOrder)

	order.LineItems = []*LineItem{}

	return order
}

func getBillingAddress(csvOrder *parsing.DnnOrderLine) *Address {
	address := &Address{}
	address.Firstname = csvOrder.BillingFirstName
	address.Lastname = csvOrder.BillingLastName
	address.Company = csvOrder.BillingCompany
	address.Street = csvOrder.BillingStreet
	address.City = csvOrder.BillingCity
	// address.Region = handleAddressRegion(csvOrder.BillingSubdivisionCode) // Contains State
	address.Postcode = csvOrder.BillingPostalCode
	address.Country = csvOrder.BillingCountryCode
	address.Telephone = csvOrder.BillingPhone
	// csvOrder.BillingEmail

	return address
}

func getShippingAddress(csvOrder *parsing.DnnOrderLine) *Address {
	address := &Address{}
	address.Firstname = csvOrder.ShippingFirstName
	address.Lastname = csvOrder.ShippingLastName
	address.Company = csvOrder.ShippingCompany
	address.Street = csvOrder.ShippingStreet
	address.City = csvOrder.ShippingCity
	// address.Region = handleAddressRegion(csvOrder.ShippingSubdivisionCode) // Contains State
	address.Postcode = csvOrder.ShippingPostalCode
	address.Country = csvOrder.ShippingCountryCode
	address.Telephone = csvOrder.ShippingPhone
	// csvOrder.ShippingEmail

	return address
}

func getUserAddress(line *parsing.DnnUserAddressLine) *Address {
	address := &Address{}
	address.Firstname = line.FirstName
	address.Lastname = line.LastName
	address.Company = line.Company
	address.Street = line.Street
	address.City = line.City
	// address.Region = handleAddressRegion(line.SubdivisionCode) // Contains State
	address.Postcode = line.PostalCode
	address.Country = line.CountryCode
	address.Telephone = line.Phone
	address.Email = line.Email

	return address
}

// CreateLineItem takes a csv line and converts it to a Magento LineItem
func CreateLineItem(csvLineItem *parsing.DnnOrderDetailsLine) *LineItem {
	lineItem := &LineItem{}

	lineItem.ID = fmt.Sprintf("DNN%v", csvLineItem.SalesOrderDetailID)
	lineItem.OrderID = csvLineItem.SalesOrderID
	lineItem.ProductSku = csvLineItem.SKU

	productName := strings.Replace(csvLineItem.ProductName, `<locale en-US="`, "", -1)
	lineItem.ProductName = strings.Replace(productName, `" />`, "", -1)

	lineItem.QtyOrdered = csvLineItem.Quantity
	lineItem.Price = csvLineItem.Price
	lineItem.RowTotal = float64(csvLineItem.Quantity) * csvLineItem.Price
	lineItem.RowWeight = float64(csvLineItem.Quantity) * csvLineItem.Weight
	lineItem.PriceInclTax = csvLineItem.Price
	lineItem.ProductTaxAmount = csvLineItem.TaxAmount1 + csvLineItem.TaxAmount2 + csvLineItem.TaxAmount3 + csvLineItem.TaxAmount4 + csvLineItem.TaxAmount5
	// lineItem.ProductOption

	return lineItem
}

// CreateUser takes a csv line and converts it to a Magento User
func CreateUser(userLine *parsing.DnnUserLine) *User {
	user := &User{}

	// user.ID = userLine.UserID
	user.DnnID = userLine.UserID
	user.Firstname = userLine.FirstName
	// user.Middlename = userLine.FirstName
	user.Lastname = userLine.LastName
	user.Email = userLine.Email
	// user.Telephone

	user.Addresses = []*Address{}

	return user
}

// CreateUserOld takes a csv line and converts it to a Magento User
func CreateUserOld(userLine *parsing.DnnUserLine) *parsing.MagentoUserLine {
	user := &parsing.MagentoUserLine{}

	user.Website = "astro"
	user.Email = userLine.Email
	user.GroupID = "General"
	// user.DisableAutoGroupChange
	user.Firstname = userLine.FirstName
	user.Lastname = userLine.LastName
	// user.PasswordHash
	// user.Telephone
	// user.URL
	// user.Newsletter
	// user.Fax
	// user.BillingPrefix
	// user.BillingFirstname
	// user.BillingMiddlename
	// user.BillingLastname
	// user.BillingSuffix
	// user.BillingStreetFull
	// user.BillingStreet1
	// user.BillingStreet2
	// user.BillingStreet3
	// user.BillingStreet4
	// user.BillingStreet5
	// user.BillingStreet6
	// user.BillingStreet7
	// user.BillingStreet8
	// user.BillingCity
	// user.BillingRegion
	// user.BillingCountry
	// user.BillingPostcode
	// user.BillingTelephone
	// user.BillingCompany
	// user.BillingFax
	// user.ShippingPrefix
	// user.ShippingFirstname
	// user.ShippingMiddlename
	// user.ShippingLastname
	// user.ShippingSuffix
	// user.ShippingStreetFull
	// user.ShippingStreet1
	// user.ShippingStreet2
	// user.ShippingStreet3
	// user.ShippingStreet4
	// user.ShippingStreet5
	// user.ShippingStreet6
	// user.ShippingStreet7
	// user.ShippingStreet8
	// user.ShippingCity
	// user.ShippingRegion
	// user.ShippingCountry
	// user.ShippingPostcode
	// user.ShippingTelephone
	// user.ShippingCompany
	// user.ShippingFax
	// user.CreatedIn
	// user.IsSubscribed
	user.Group = "General"
	// user.Confirmation
	// user.RpToken
	// user.RpTokenCreatedAt
	// user.Middlename

	return user
}

func AddAddressesToUser(addresses []*parsing.DnnUserAddressLine, users []*User) {
	m := make(map[string]*User)

	for _, user := range users {
		m[user.DnnID] = user
	}

	for _, addressLine := range addresses {
		address := getUserAddress(addressLine)
		u := m[addressLine.UserID]
		if u != nil {
			u.Addresses = append(u.Addresses, address)
		}

	}

}

func handleAddressRegion(region string) string {
	if strings.Index(region, "-") >= 0 {
		return strings.Split(region, "-")[1]
	}
	return region
}
