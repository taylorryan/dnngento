package magento

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"github.com/dghubble/oauth1"
)

// http://cms-tools.test:8080/index.php/manage/oauth_authorize?oauth_token=7633c3159c94dc8dcfb9612ec5a1bc3c&oauth_token_secret=6fa9bc087c3046a62994fb62dc8e6fe2

func ApiPostUsers() {

	config := oauth1.NewConfig("a4eb21525c1c591bdaae3f61abfc2df0", "a2ce074e46b2b9fea54e99baa0dc8935")
	token := oauth1.NewToken("95d67ffa738a80a53759d01f50a1c625", "2bd400c97a7092b1b9e7bd216854c861")

	httpClient := config.Client(oauth1.NoContext, token)

	body := &map[string]interface{}{
		"firstname":  "Test",
		"lastname":   "User",
		"email":      "test@test.com",
		"password":   "taylorryan2",
		"website_id": 2,
		"group_id":   1,
	}
	bytesRepresentation, err := json.Marshal(body)

	path := "http://astrotool.test:8080/api/rest/customers"

	req, err := http.NewRequest("POST", path, bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Accept", "application/json")

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	var target map[string]map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&target)
	log.Print(target)

	// return processUsers(target)
}

func ApiGetUsers() map[string]map[string]interface{} {

	config := oauth1.NewConfig("a4eb21525c1c591bdaae3f61abfc2df0", "a2ce074e46b2b9fea54e99baa0dc8935")
	token := oauth1.NewToken("95d67ffa738a80a53759d01f50a1c625", "2bd400c97a7092b1b9e7bd216854c861")

	httpClient := config.Client(oauth1.NoContext, token)

	path := "http://astrotool.test:8080/api/rest/customers?limit=100000"

	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Accept", "application/json")

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	var target map[string]map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&target)

	return processUsers(target)
}

func processUsers(json map[string]map[string]interface{}) map[string]map[string]interface{} {
	keys := make([]string, 0, len(json))
	users := make(map[string]map[string]interface{})
	for k, u := range json {
		// Get users in astro tool site.
		if u["website_id"] == "2" {
			keys = append(keys, k)
			users[u["email"].(string)] = u
		}

	}

	return users
}
