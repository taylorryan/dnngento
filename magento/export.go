package magento

import (
	"os"
	"strconv"

	"gitlab.com/taylorryan/dnngento/parsing"

	"github.com/gocarina/gocsv"
)

// WriteMagentoImportFile takes orders and writes them to a csv to be imported into magento.
func WriteMagentoImportFile(orders []*Order) {

	importFile, err := os.OpenFile("magento-orders.csv", os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer importFile.Close()

	importLines := ordersToCsvLines(orders)
	err = gocsv.MarshalFile(&importLines, importFile) // Use this to save the CSV back to the file
	if err != nil {
		panic(err)
	}
}

func ordersToCsvLines(orders []*Order) []*parsing.MagentoImportLine {
	csvLines := []*parsing.MagentoImportLine{}

	for _, order := range orders {
		for i, lineitem := range order.LineItems {
			csv := &parsing.MagentoImportLine{}

			if i == 0 {
				// Special "first line" things
				csv.OrderID = order.ID
				csv.Email = order.Email
				csv.Firstname = order.Firstname
				csv.Lastname = order.Lastname
				csv.Prefix = order.BillingAddress.Prefix
				csv.Middlename = order.BillingAddress.Middlename
				csv.Suffix = order.BillingAddress.Suffix
				// csv.Taxvat
				csv.CreatedAt = order.CreatedAt
				csv.UpdatedAt = order.UpdatedAt
				// csv.InvoiceCreatedAt = order.CreatedAt
				// csv.ShipmentCreatedAt = order.CreatedAt
				// csv.CreditmemoCreatedAt
				csv.TaxAmount = order.TaxAmount
				csv.BaseTaxAmount = order.TaxAmount
				csv.DiscountAmount = order.DiscountAmount
				csv.BaseDiscountAmount = order.DiscountAmount
				csv.ShippingTaxAmount = order.ShippingTaxAmount
				csv.BaseShippingTaxAmount = order.ShippingTaxAmount
				csv.BaseToGlobalRate = order.BaseToGlobalRate
				csv.BaseToOrderRate = order.BaseToOrderRate
				csv.StoreToBaseRate = order.StoreToBaseRate
				csv.StoreToOrderRate = order.StoreToOrderRate
				csv.SubtotalInclTax = order.SubtotalInclTax
				csv.BaseSubtotalInclTax = order.SubtotalInclTax
				csv.CouponCode = order.CouponCode
				csv.ShippingInclTax = order.ShippingInclTax
				csv.BaseShippingInclTax = order.ShippingInclTax
				csv.ShippingMethod = order.ShippingMethod
				csv.ShippingAmount = order.ShippingAmount
				csv.Subtotal = order.Subtotal
				csv.BaseSubtotal = order.Subtotal
				csv.GrandTotal = order.GrandTotal
				csv.BaseGrandTotal = order.GrandTotal
				csv.BaseShippingAmount = order.ShippingAmount
				csv.AdjustmentPositive = 0
				csv.AdjustmentNegative = 0
				csv.RefundedShippingAmount = 0
				csv.BaseRefundedShippingAmount = 0
				csv.RefundedSubtotal = 0
				csv.BaseRefundedSubtotal = 0
				csv.RefundedTaxAmount = 0
				csv.BaseRefundedTaxAmount = 0
				csv.RefundedDiscountAmount = 0
				csv.BaseRefundedDiscountAmount = 0
				csv.StoreID = "2"
				csv.OrderStatus = "complete"
				csv.OrderState = "complete"
				// csv.HoldBeforeState
				// csv.HoldBeforeStatus
				csv.StoreCurrencyCode = "USD"
				csv.BaseCurrencyCode = "USD"
				csv.OrderCurrencyCode = "USD"
				// csv.TotalPaid = order.GrandTotal
				// csv.BaseTotalPaid = order.GrandTotal
				csv.IsVirtual = 0
				csv.TotalQtyOrdered = order.TotalQtyOrdered
				// csv.RemoteIP
				// csv.TotalRefunded
				// csv.BaseTotalRefunded
				// csv.TotalCanceled
				csv.TotalInvoiced = order.GrandTotal
				csv.BillingCustomerID = strconv.Itoa(order.CustomerID)
				csv.BillingPrefix = order.BillingAddress.Prefix
				csv.BillingFirstname = order.BillingAddress.Firstname
				csv.BillingMiddlename = order.BillingAddress.Middlename
				csv.BillingLastname = order.BillingAddress.Lastname
				csv.BillingSuffix = order.BillingAddress.Suffix
				csv.BillingStreetFull = order.BillingAddress.Street
				csv.BillingCity = order.BillingAddress.City
				csv.BillingRegion = order.BillingAddress.Region
				csv.BillingCountry = order.BillingAddress.Country
				csv.BillingPostcode = order.BillingAddress.Postcode
				csv.BillingTelephone = order.BillingAddress.Telephone
				csv.BillingCompany = order.BillingAddress.Company
				csv.BillingFax = order.BillingAddress.Fax
				csv.ShippingCustomerID = strconv.Itoa(order.CustomerID)
				csv.ShippingPrefix = order.ShippingAddress.Prefix
				csv.ShippingFirstname = order.ShippingAddress.Firstname
				csv.ShippingMiddlename = order.ShippingAddress.Middlename
				csv.ShippingLastname = order.ShippingAddress.Lastname
				csv.ShippingSuffix = order.ShippingAddress.Suffix
				csv.ShippingStreetFull = order.ShippingAddress.Street
				csv.ShippingCity = order.ShippingAddress.City
				csv.ShippingRegion = order.ShippingAddress.Region
				csv.ShippingCountry = order.ShippingAddress.Country
				csv.ShippingPostcode = order.ShippingAddress.Postcode
				csv.ShippingTelephone = order.ShippingAddress.Telephone
				csv.ShippingCompany = order.ShippingAddress.Company
				csv.ShippingFax = order.ShippingAddress.Fax
				csv.PaymentMethod = order.PaymentMethod
			}

			csv.ProductSku = lineitem.ProductSku
			csv.ProductName = lineitem.ProductName
			csv.QtyOrdered = lineitem.QtyOrdered
			csv.QtyInvoiced = lineitem.QtyOrdered
			csv.QtyShipped = lineitem.QtyOrdered
			csv.QtyRefunded = 0
			csv.QtyCanceled = 0
			csv.ProductType = "simple"
			csv.OriginalPrice = lineitem.Price
			csv.BaseOriginalPrice = lineitem.Price
			csv.RowTotal = lineitem.RowTotal
			csv.BaseRowTotal = lineitem.RowTotal
			csv.RowWeight = lineitem.RowWeight
			csv.PriceInclTax = lineitem.PriceInclTax
			csv.BasePriceInclTax = lineitem.PriceInclTax
			csv.ProductTaxAmount = 0
			csv.ProductBaseTaxAmount = 0
			csv.ProductTaxPercent = 0
			csv.ProductDiscount = 0
			csv.ProductBaseDiscount = 0
			csv.ProductDiscountPercent = 0
			csv.IsChild = "no"
			csv.ProductOption = ""

			csvLines = append(csvLines, csv)

		}
	}

	return csvLines
}

// WriteMagentoUserImportFile takes users and writes them to a csv to be imported into magento.
func WriteMagentoUserImportFile(users []*User) {

	importFile, err := os.OpenFile("magento-users.csv", os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer importFile.Close()

	importLines := userToCsvLines(users)

	err = gocsv.MarshalFile(&importLines, importFile) // Use this to save the CSV back to the file
	if err != nil {
		panic(err)
	}
}

func userToCsvLines(users []*User) []*parsing.MagentoUserImportLine {
	csvLines := []*parsing.MagentoUserImportLine{}

	for _, user := range users {
		for i, address := range user.Addresses {

			csv := &parsing.MagentoUserImportLine{}

			if i == 0 {
				// Special "first line" things
				csv.Email = user.Email
				csv.Website = "astro"
				csv.Store = "astro"
				// csv.CreatedAt
				// csv.CreatedIn
				csv.Firstname = user.Firstname
				csv.GroupID = 1
				csv.Lastname = user.Lastname
				csv.Middlename = user.Middlename
				csv.StoreID = 2
				csv.Telephone = user.Telephone
				csv.WebsiteID = 2
			}

			csv.AddressCity = address.City
			csv.AddressCompany = address.Company
			csv.AddressCountryID = address.Country
			csv.AddressFax = address.Fax
			csv.AddressFirstname = address.Firstname
			csv.AddressLastname = address.Lastname
			csv.AddressMiddlename = address.Middlename
			csv.AddressPostcode = address.Postcode
			csv.AddressPrefix = address.Prefix
			csv.AddressRegion = address.Region
			csv.AddressStreet = address.Street
			csv.AddressSuffix = address.Suffix
			csv.AddressTelephone = address.Telephone
			// csv.AddressVatID = address.Vat
			// csv.AddressDefaultBilling = address.City
			// csv.AddressDefaultShipping = address.City
			csvLines = append(csvLines, csv)
		}
	}

	return csvLines
}
